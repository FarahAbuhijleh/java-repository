<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin web page</title>
<link rel="stylesheet" href="../CSS/style.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="../CSS/style.css">
<link rel="stylesheet" href="../CSS/logIn.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script type="text/javascript" src="../js/function.js">
</script>
<title>Insert title here</title>
</head>
<style>
.mySlides {display:none;}
</style>
<body>
<h2 id="divHeader"> Employee food services  </h2>
<div class="w3-sidebar w3-sand" >
<div id="divAdmin">
  <a href="../UI/AddFoodForm.jsp" class="w3-bar-item w3-button ">Add Food Service </a><br><br>
  <a href="/STsService/showRestaurant"  class="w3-bar-item w3-button ">Show Food Service </a><br><br>
  <a href=""  class="w3-bar-item w3-button ">Edit Food Service </a><br><br>
  <a href="/STsService/select" class="w3-bar-item w3-button">Indicate Restaurant</a><br><br>
    <a href="/STsService/showOrder"  class="w3-bar-item w3-button ">Show Orders List</a></br><br>
   <a href="../UI/firstPage.jsp"  class="w3-bar-item w3-button ">Exit </a>
</div>
</div>
<div id ="divImages" >
    <img class="mySlides" src="../Images/mburger.jpg"  width=100% height=100%>
  <img class="mySlides" src="../Images/salad2.jpg"  width=100% height=100%>
   <img class="mySlides" src="../Images/pizza.jpg"  width=100% height=100%>
  <img class="mySlides" src="../Images/falfel.jpg"  width=100% height=100%>
    <img class="mySlides" src="../Images/mc.jpg"  width=100% height=100%>
      <img class="mySlides" src="../Images/sh.jpg"  width=100% height=100%>
        <img class="mySlides" src="../Images/pasta2.jpg"  width=100% height=100%>
     <img class="mySlides" src="../Images/juice.jpg"  width=100% height=70%>
    
</div>
<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 3000); // Change image every 3 seconds
}
</script>
</body>
</html>