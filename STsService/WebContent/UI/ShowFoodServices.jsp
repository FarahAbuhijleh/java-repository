<%@page import="com.sts.service.module.Restaurants"%>
<%@page import="com.sts.service.module.Menu"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ page import="java.net.*, java.io.*, java.sql.*, java.util.*"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="./CSS/style.css">
<link rel="stylesheet" href="./CSS/logIn.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script type="text/javascript" src="../js/function.js"></script>
<title>Show Food Services</title>
</head>
<body>
<div class="w3-sidebar w3-sand" >
  <a href="/STsService/UI/AdminForm1.jsp" class="w3-bar-item w3-button ">Home</a><br><br>
  </div>
<form  class="white-pink"  method="post" action="/STsService/showRestaurant">
<h1>Select Restaurant</h1><br>
<select name="resName">
<%List<Restaurants> l=(List<Restaurants>)request.getAttribute("list"); 
for(Restaurants x:l){
%>
<option ><%=x.getNameResturant()%></option>
<%} %>
</select><br>
<input type="submit" value="SHOW">
</form>

<div class="white-pink">
<%
String name=(String)request.getAttribute("name");
if(name==null){%>
	<h1>The Menu :</h1>
<%}else{%>
	<h1>The Menu of <%=name%>:</h1>
	<table border="2"  ALIGN=CENTER bordercolor="#f4c8db" cellpadding="20" width= "100%" >
<thead>
<tr>
<th>Name</th>
<th>Price</th>
<th> Type</th><br>
</tr>
</thead >
 <%Restaurants object=(Restaurants)request.getAttribute("objectRes");
 for (Menu s : object.getListOfMenu())
 {%>
<tr><td><%=s.getName()%></td><td><%=s.getPrice()+" JD"%><td><%=s.getCategory()%></tr>
 <% }
 %>
<% }%>
</table>
</div>
</body>
</html>