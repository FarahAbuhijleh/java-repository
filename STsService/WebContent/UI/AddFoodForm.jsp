<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Food Service </title>
<link rel="stylesheet" href="../CSS/logIn.css" />
<link rel="stylesheet" href="../CSS/style.css" />
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<div class="w3-sidebar w3-sand" >
  <a href="../UI/AdminForm1.jsp" class="w3-bar-item w3-button ">Home</a></br></br>
  </div>
<form method="post" action="/STsService/addService">
<div class="white-pink"><br><h1> The restaurant </h1><br>
<input type="text" name="resName" placeholder="Name of Restaurant " pattern="^[a-zA-Z ]*$"  title="Just Characters"  required  />
</div> <br>
<div class="white-pink"><br><h1> The restaurant menu</h1><br>
<input type="text"  name="mealName" placeholder="Name" pattern="^[a-zA-Z ]*$" title="Just Characters" required  /><br>
 </span></lable><input type="text" name="price"  placeholder="Price" pattern="\d+(\.\d{2})?"  title="Insert Correct Price" required /><br><br>
<lable> <span id="type">Choose type : </span></lable><br><br>
<input type="radio" name="type" value="Sandwich" checked><lable> <span>Sandwich</span></lable><br>
<input type="radio" name="type" value="Juice"><lable> <span>Juice  </span></lable><br>
<input type="radio" name="type" value="Meal"><lable> <span>Meal  </span></lable><br>
<input type="radio" name="type" value="Salad"><lable> <span>Salad  </span></lable><br>
<div>
<input type="Submit" value="ADD"/>
</div>
</div>
</form>
</body>
</html>