<%@page import="com.sts.service.module.Order"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.net.*, java.io.*, java.sql.*, java.util.*"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="./CSS/style.css">
<link rel="stylesheet" href="./CSS/logIn.css">
<title>show orders list</title>
</head>
<body>
<div class="w3-sidebar w3-sand" >
  <a href="/STsService/UI/AdminForm1.jsp" class="w3-bar-item w3-button ">Home</a><br><br>
  </div>
  <div class="white-pink">
<%List<Order>list=(List<Order>)request.getAttribute("OrderList");%>
	<h1>List Of Orders </h1>
	<table border="2"  ALIGN=CENTER bordercolor="#f4c8db" cellpadding="20" width= "100%" >
<thead>
<tr>
<th>Email</th>
<th>Orders</th>
</tr>
</thead >
<% for(Order s : list)
 {%>
<tr><td><%=s.getEmployee().getEmail()%></td><td><%=s.getOrders()%></tr>
 <% }
 %>
</table>
</div>
</body>
</html>