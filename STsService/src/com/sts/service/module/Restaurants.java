package com.sts.service.module;


import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="Resturants")
public class Restaurants {

	@Id @GeneratedValue
	@Column(name="id")
	private int id;
	
	
	@Column(name="nameResturant")
	private String nameResturant;
	
	@Column(name="Active")
	private String active;
	
	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	@OneToMany(mappedBy="resturant")
	private Set<Menu> ListOfMenu ;
	public Set<Menu> getListOfMenu() {
		return ListOfMenu;
	}

	public void setListOfMenu(Set<Menu> listOfMenu) {
		ListOfMenu = listOfMenu;
	}

	@Override
	public String toString() {
		return "Restaurants [nameResturant=" + nameResturant + "]";
	}

	public Restaurants(int id, String nameResturant,String active) {
		super();
		this.id = id;
		this.active=active;
		this.nameResturant = nameResturant;
	}

	public Restaurants() {
		super();
	}

	public Restaurants(String nameResturant,String active) {
		this.nameResturant = nameResturant;
		this.active=active;
	}

	public Restaurants(String nameResturant) {
		this.nameResturant = nameResturant;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNameResturant() {
		return nameResturant;
	}

	public void setNameResturant(String nameResturant) {
		this.nameResturant = nameResturant;
	}
}
