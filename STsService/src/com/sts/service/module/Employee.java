package com.sts.service.module;


import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table (name="Employee")
public class Employee {
	
	@Id @GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="email")
	private String email;
	
	@Column(name="active")
	private String active;

	
	@OneToMany(mappedBy="employee")
	private Set<Order> ListOfOrders ;
	
	
	public Set<Order> getListOfOrders() {
		return ListOfOrders;
	}

	public void setListOfOrders(Set<Order> listOfOrders) {
		ListOfOrders = listOfOrders;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Employee(String email) {
		super();
		this.email = email;
	}

	public Employee(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Column(name="Department")
	private String department;
	
     @Column(name="password")
	 private String password;
	  
	public Employee(String email, String department, String password,String active) {
		super();
		this.email = email;
		this.department = department;
		this.password = password;
		this.active=active;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Employee(){
		}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	
	
	
	
	

}
