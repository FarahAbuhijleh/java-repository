package com.sts.service.module;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="OrderTB")
public class Order {

	@Id @GeneratedValue
	@Column(name="Id")
	private  int id;

	@Column(name="Orders")
	private String orders;
	
	@Column(name="Confirm")
	private String confirm;

	
	@Column (name="date")
	private String date;
	
	@ManyToOne
	@JoinColumn(name="EmployeeId")
	private Employee employee;
	
	public Order(){
		
	}
	
	public Order(int id) {
		super();
		this.id = id;
	}

	public Order( String orders, String confirm, Employee employee, String d) {
		super();
	
		this.orders = orders;
		this.confirm = confirm;
		this.employee = employee;
		this.date=d;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrders() {
		return orders;
	}

	public void setOrders(String orders) {
		this.orders = orders;
	}

	public String getConfirm() {
		return confirm;
	}

	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	
	
}
