package com.sts.service.module;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Menu")
public class Menu {

	
	@Id @GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="price")
	private double price;
	
	
	@Column(name="Category")
	private String category;
	
	@ManyToOne
	@JoinColumn(name="resturantId")
	private Restaurants resturant;
	
	
	
	
	public Menu() {
		super();
	}
	public Menu(int id){
		this.id=id;
	}
	
	public Menu(Restaurants resturant) {
	
		this.resturant = resturant;
	}

	public Menu(String name, double price, String category) {
		super();
		this.name = name;
		this.price = price;
		this.category = category;
	}

	public Menu(String name, double price, String category, Restaurants resturant) {
		super();
		this.name = name;
		this.price = price;
		this.category = category;
		this.resturant = resturant;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Restaurants getResturant() {
		return resturant;
	}

	public void setResturant(Restaurants resturant) {
		this.resturant = resturant;
	}
	@Override
	public String toString() {
		return "Menu [id=" + id + ", name=" + name + ", price=" + price
				+ ", category=" + category + ", resturant=" + resturant + "]";
	}
	
	
	
}
