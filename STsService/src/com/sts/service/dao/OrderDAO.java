package com.sts.service.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.sts.service.module.Employee;
import com.sts.service.module.Menu;
import com.sts.service.module.Order;
import com.sts.service.module.Restaurants;

public class OrderDAO {
	private static SessionFactory factory=new Configuration().
			configure("\\com\\sts\\service\\connection\\hibernate.cfg.xml").addAnnotatedClass(Order.class).buildSessionFactory();
	
	 org.hibernate.Session session =factory.openSession();
	 org.hibernate.Transaction tx = null;
	 public int addOrder(Order order){	
		 int id=0;
	      try{
	         tx =  session.beginTransaction(); 
	        // System.out.print("added");
	         session.save(order); 
	         id=order.getId();
	         System.out.println(id);
	         tx.commit();
	         return id;
	        
	      } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
			return -1;
		}finally {
	         session.close(); 
	      }
}
	
	
	
	
	 public boolean activeOrder(Order order){
			try{
				tx = session.beginTransaction();
	        org.hibernate.Query query=session.createQuery("from Order order where order.id='"+order.getId()+"'");
	        for(int i=0;i<query.list().size();i++){
	       	Order order1=(Order)query.list().get(i);
	        order1.setConfirm("1");
	        session.update(order1);}
	        tx.commit();	
	        return true;}
			catch(Exception e){
				 e.printStackTrace(); 
				 return false;
			}finally {
		         session.close(); 
			}
			
		}
	 public List<Order> selectOrder(Order ord){
		System.out.println("hello");	
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		 Date d=new Date();
		 System.out.println(d);
		 System.out.println(sdf.format(d));
		 Criteria cr=session.createCriteria(Order.class);
		 cr.add(Restrictions.eq("confirm","1"));
		 cr.add(Restrictions.eq("date",sdf.format(d)));
		 return cr.list();
		 
	}
	
	
	
}
