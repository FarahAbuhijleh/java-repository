package com.sts.service.dao;
import java.util.List;

import javax.management.Query;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import com.sts.service.algorithms.CipherHelper;
import com.sts.service.module.Employee;



public class EmployeeDAO {

	private static SessionFactory factory=new Configuration().
	   configure("\\com\\sts\\service\\connection\\hibernate.cfg.xml").addAnnotatedClass(Employee.class).buildSessionFactory();
	
	   //open session 
	 org.hibernate.Session session =factory.openSession();
	 org.hibernate.Transaction tx = null;
	 
	public boolean addEmployeeDAO(Employee emp){
		    try{
		         tx =  session.beginTransaction(); 
		         session.save(emp); 
		         tx.commit();
		         return true;
		      } catch (Exception e) {
				// TODO Auto-generated catch block
				 e.printStackTrace(); 
				 return false;
			}finally {
		         session.close(); 
		      }
				
	}
	
	
	
	public 	boolean CheckEmployee(Employee emp){
		
		 String hql = "From Employee emp WHERE emp.email='"+emp.getEmail()+"'and emp.password='"+emp.getPassword()+"'";
		 org.hibernate.Query query = session.createQuery(hql);
		 List<Employee> results = query.list();
		if(results.isEmpty()){
			return false;}
		else{
			return true;}
		
	}

	public boolean checkEmail(Employee emp){
	
		 String hql="From Employee emp Where emp.email='"+emp.getEmail()+"'";
		 org.hibernate.Query query=session.createQuery(hql);
		 List<Employee>results=query.list();
		 if(results.isEmpty()){
			 return true;
		 }else{
			 return false;
		 }	
	}
	
	public String Selectpass(Employee emp){
		String pass=(String) session.createQuery("select emp.password from Employee emp where emp.email='"+emp.getEmail()+"'").uniqueResult();
     	 return pass;
	}
	
	
	public boolean activeEmployee(Employee emp){
		try{
			tx = session.beginTransaction();
        org.hibernate.Query query=session.createQuery("from Employee emp where emp.email='"+emp.getEmail()+"'");
        for(int i=0;i<query.list().size();i++){
       	Employee emp1=(Employee)query.list().get(i);
        emp1.setActive("1");
        session.update(emp1);}
        tx.commit();	
        return true;}
		catch(Exception e){
			 e.printStackTrace(); 
			 return false;
		}finally {
	         session.close(); 
		}
		
	}
	
	public int checkActiveOfEmail(Employee emp){
		try{
		       String query=(String) session.createQuery("select emp.active from Employee emp where emp.email='"+emp.getEmail()+"'").uniqueResult();
	           if(query.equals("0")){
	        	   return 0;
	           }else{
	        	   return 1;
	           }
		}
		catch(Exception e){
			return -1;
			
		}
	}
	public Employee selectEmp(Employee emp){
		String hql=(String)"from Employee emp where emp.email='"+emp.getEmail()+"'";
		org.hibernate.Query query=session.createQuery(hql);
		List results=query.list();
		return (Employee)results.get(0);
	}
	
	public Employee returnPass(String email){
		Criteria cr=session.createCriteria(Employee.class);
		cr.add(Restrictions.eq("email", email));
		return (Employee) cr.list();
	}
	
}









