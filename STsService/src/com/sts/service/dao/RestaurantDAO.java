package com.sts.service.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.sts.service.module.Restaurants;
public class RestaurantDAO {	
	
	private static SessionFactory factory=new Configuration().
		configure("\\com\\sts\\service\\connection\\hibernate.cfg.xml").addAnnotatedClass(Restaurants.class).buildSessionFactory();
	
	org.hibernate.Session session =factory.openSession();
	 org.hibernate.Transaction tx = null;
	 
	public boolean addRestaurantDAO(Restaurants res){
	
		      try{

		        // System.out.print("added");
		         session.save(res); 
		         tx.commit();
		        return true;
		      } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 
				return false;
			}finally {
		         session.close(); 
		      }
				
}
	public Restaurants selectActive(Restaurants res){
		
			tx=session.beginTransaction();
			String active="1";
			String hql="from Restaurants  res where res.active='"+active+"'";
			org.hibernate.Query query = session.createQuery(hql);
			List  results = query.list();
		    return (Restaurants) results.get(0);
	}
	
	
	public int selectID(Restaurants res){
		
		try{
		 tx=session.beginTransaction();
		 int x=0;
		 String hql = "select res.id FROM Restaurants res WHERE res.nameResturant='"+res.getNameResturant()+"'";
		 org.hibernate.Query query = session.createQuery(hql);
		 List results = query.list();
		 Iterator iterator = results.iterator();
         while(iterator.hasNext()){
            x= (int) iterator.next();
            }
          return x;}
		catch(Exception e){
			e.printStackTrace(); 
			return -1;
		}
	
	
}
	public Restaurants selectRes(Restaurants res){
		
		org.hibernate.Session session =factory.openSession();
		
		 tx=session.beginTransaction();
		 String hql = " from Restaurants res WHERE res.nameResturant='"+res.getNameResturant()+"'";
		 org.hibernate.Query query = session.createQuery(hql);
		 List  results = query.list();
		
		return (Restaurants) results.get(0);
	}
	
	public List<Restaurants> selectAll(){
		
		org.hibernate.Session session =factory.openSession();
		
		 Query query = session.createQuery("from Restaurants"); 
		
		 return query.list(); 
	}
	
	public boolean updateAll(){
	
		try{
		tx= session.beginTransaction();
		
         org.hibernate.Query query=session.createQuery("from Restaurants");
         for(int i=0;i<query.list().size();i++){
        	Restaurants res1=(Restaurants)query.list().get(i);
            res1.setActive("0");
            session.update(res1);
          }
         tx.commit();
         return true;}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	public boolean updateRow(Restaurants res){
		try{
		
		tx= session.beginTransaction();
         org.hibernate.Query query=session.createQuery("from Restaurants res where res.id='"+res.getId()+"'");
         for(int i=0;i<query.list().size();i++){
        	Restaurants res1=(Restaurants)query.list().get(i);
            res1.setActive("1");
            session.update(res1);
          }
         tx.commit();
         return true;}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	
}