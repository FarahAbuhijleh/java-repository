package com.sts.service.dao;


import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.sts.service.module.Employee;
import com.sts.service.module.Menu;



public class MenuDAO {

	
	private static SessionFactory factory=new Configuration().
			configure("\\com\\sts\\service\\connection\\hibernate.cfg.xml").addAnnotatedClass(Menu.class).buildSessionFactory();
	
	 org.hibernate.Session session =factory.openSession();
	 org.hibernate.Transaction tx = null;
	 public boolean addMenuDAO(Menu menu){
		
		      try{
		         tx =  session.beginTransaction(); 
		        // System.out.print("added");
		         session.save(menu); 
		         tx.commit();
		         return true;
		        
		      } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 
				return false;
			}finally {
		         session.close(); 
		      }
	
	
	}
	 public Menu selectMenu(Menu m){
		 String hql=(String)"from Menu m where m.id='"+m.getId()+"'";
			org.hibernate.Query query=session.createQuery(hql);
			List results=query.list();
			return (Menu)results.get(0);
	 }
	
	
	}
