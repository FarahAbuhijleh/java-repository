package com.sts.service.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.sts.service.algorithms.CipherHelper;
import com.sts.service.dao.OrderDAO;
import com.sts.service.module.Email;
import com.sts.service.module.Employee;
import com.sts.service.module.Order;


public class OrderController {

	OrderDAO ordDAO=new OrderDAO();
	public int addOrder(Employee emp,String orders,String confirm,Date d){
		try{
		
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			 
		Order order=new Order(orders, confirm, emp,sdf.format(d));
		System.out.println("Order in controller after create object "+order.getDate());
		return  ordDAO.addOrder(order);
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
		
	}
	public boolean sendEmail(String email,int id,String Order ,double price){	
	    String subject ="Confirm Order ,STS FOOD SERVICE ";
        String  text="Click  to activate your Orders in STS Food service "+"\n \n Your Orders :\n"+Order+"\n\nTotal Price :"+price;
        String stringID=id+"";
        String idEn=null;
		try {
			idEn = CipherHelper.cipher("ordeerID", stringID);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        String  url="http://172.22.100.86:8086/STsService/verify?id="+idEn;
	    Email emailObj=new Email();	
	    return emailObj.sendMail(email, subject, text, url); 
        
	}
	
	public boolean activeOrder(int id){
	      
        try{
        Order order=new Order(id);
        return  ordDAO.activeOrder(order);
		 }
		  catch(Exception e){
			  return false;
		  }
}
	 public List<Order> selectOrder(){
			Order order=new Order();
		    return ordDAO.selectOrder(order) ;
	}
	
	 
	
}
