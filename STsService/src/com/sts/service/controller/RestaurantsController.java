package com.sts.service.controller;
import java.util.List;

import com.sts.service.dao.RestaurantDAO;
import com.sts.service.module.Restaurants;

public class RestaurantsController {

	RestaurantDAO resDAO=new RestaurantDAO();
	public int selectID(String name,String active){
		
		Restaurants res=new Restaurants(name,active);
		
		int id=resDAO.selectID(res);	
    	return id;
}
	
	public boolean addRestaurant(Restaurants res){
		return resDAO.addRestaurantDAO(res);
	}
	
	public Restaurants selectRes(String name){
		Restaurants res=new Restaurants(name);
		 return resDAO.selectRes(res);
	}
	
	public List<Restaurants> selectAll(){
		 return resDAO.selectAll();
	}
	public boolean updateRow(Restaurants res){
		return resDAO.updateRow(res);
	}
	public boolean updateAll(){
		return resDAO.updateAll();
	}
	public Restaurants selectResActive(){
		Restaurants res=new Restaurants();
		return resDAO.selectActive(res);
	}
}
