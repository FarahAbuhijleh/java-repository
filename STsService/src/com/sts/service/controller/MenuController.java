package com.sts.service.controller;

import java.util.HashSet;
import java.util.Set;

import com.sts.service.dao.MenuDAO;
import com.sts.service.module.Menu;
import com.sts.service.module.Restaurants;


public class MenuController {

	MenuDAO menuDAO=new MenuDAO();
	public boolean addMenu(Menu menu){
		try{
			
			menuDAO.addMenuDAO(menu);
			
		return true;}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	public boolean addMenuToRest(String name,double price,String type,Restaurants res){
		
		try{
		Menu menu=new Menu(name, price, type);
		Set<Menu> menuList = new HashSet<Menu>();
		menuList.add(menu);
		res.setListOfMenu(menuList);
		menu.setResturant(res);
		boolean isAdded=addMenu(menu);
		return isAdded;}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	
	public Menu selectMenu(int id){
		Menu menu=new Menu(id);
		return menuDAO.selectMenu(menu);
		
	}
}
