package com.sts.service.controller;
import com.sts.service.algorithms.CipherHelper;
import com.sts.service.dao.EmployeeDAO;
import com.sts.service.module.Email;
import com.sts.service.module.Employee;

import sun.misc.BASE64Encoder;

public class EmployeeController {
	
	 EmployeeDAO empDAO=new EmployeeDAO();
	 
	public String encryptPassword(String password){
		
		  BASE64Encoder encoder = new BASE64Encoder();
	       String pass = new String(encoder.encodeBuffer(password.getBytes()));
		//   System.out.println("Encrypted password : "+str);
		   return pass;
	}
	
	public boolean checkEmail(String email){
		  Employee empEmail=new Employee(email);
		  boolean check=empDAO.checkEmail(empEmail);
		  return check;
	} 
	
	public boolean addEmployee(String email,String department,String password,String active){
		     try{     
				 Employee emp=new Employee(email,department,password,active);
				 return empDAO.addEmployeeDAO(emp);
				 }
				  catch(Exception e){
					  return false;
				  }
	}
	public boolean sendEmail(String email){	
	    String subject ="Activation email ";
        String  text="Click here to activate your account in STS Food service ";
        String cipherEmail=null,url=null;
        try {
			 cipherEmail=CipherHelper.cipher("emailEmp", email);
			 url="http://172.22.100.86:8086/STsService/activeAccount?email="+cipherEmail;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      
	    Email emailObj=new Email();	
	    return emailObj.sendMail(email, subject, text, url); 
	}
	
	public boolean activeEmployee(String email){
		      
		        try{
	            Employee emp=new Employee(email);
	            return  empDAO.activeEmployee(emp);
				 }
				  catch(Exception e){
					  return false;
				  }
	}
	public String Selectpass(String email){
		Employee emp=new Employee(email);
     	 return empDAO.Selectpass(emp);
	}
		public boolean checkEmployee(String email,String password){
			  
			Employee emp=new Employee(email,password);
			return empDAO.CheckEmployee(emp);
			
		}
		public int checkActiveOfEmail(String email,String password){
			Employee emp=new Employee(email, password);
			return empDAO.checkActiveOfEmail(emp);
		}
		
		public Employee selectEmp(String email){
			Employee emp=new Employee(email);
			return empDAO.selectEmp(emp);
			
		}
		
		public String returnPass(String email){
		return	empDAO.returnPass(email).getPassword();
		}
		}
