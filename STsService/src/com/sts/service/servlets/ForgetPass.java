package com.sts.service.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sts.service.algorithms.CipherHelper;
import com.sts.service.controller.EmployeeController;
import com.sts.service.module.Email;

/**
 * Servlet implementation class ForgetPass
 */
@WebServlet("/ForgetPass")
public class ForgetPass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ForgetPass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();
		out.println("<script type=\"text/javascript\">");
		
		String email=request.getParameter("email");
		/*
		 * check if email is exist 
		 * check if email is active
		 * select password of email 
		 * decipher password 
		 * send password to user's email
		*/
		
		EmployeeController controllerEmp=new EmployeeController();
		boolean exist=controllerEmp.checkEmail(email);
		System.out.println(exist);
		/*
		 * exist is true if the email does'nt in database,false : Exist in data base
		 */
		if(!exist){
			/*
			 * select password from data base and save it in passwordEn variable
			 * password variable has password of user after Decipher
			 */
		String passwordEn=controllerEmp.Selectpass(email);
		String password=null;
		try {
			password=CipherHelper.decipher("password", passwordEn);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(password);
			int checkActive =controllerEmp.checkActiveOfEmail(email, password);
			if(checkActive==1){
				System.out.println("active");
				Email obj=new Email();
				boolean isSend=obj.sendMail(email,"password","Your account password is :"+password,"");
				if(isSend){
					out.println("alert('Check your email ')");
					out.println("location='/STsService/UI/firstPage.jsp';");
				}else{
					out.println("alert('problem has occured')");
					out.println("location='/STsService/UI/forgetPassword.jsp';");
				
				}
				
			}else{
				out.println("alert('Your account does not active ,please check your email to activate your account')");
				out.println("location='/STsService/UI/forgetPassword.jsp';");
			
			}
			
	}else{
		out.println("alert('Sorry, we do not recognize this email ')");
		out.println("location='/STsService/UI/forgetPassword.jsp';");
	}
		out.println("</script>");
	}
		
	}

