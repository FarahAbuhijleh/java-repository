package com.sts.service.servlets;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sts.service.algorithms.CipherHelper;
import com.sts.service.controller.EmployeeController;




@WebServlet("/ActivateAccount.")
public class ActivateAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public ActivateAccount() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  PrintWriter out =response.getWriter();
		  out.println("<script type=\"text/javascript\">");
		  //Get email from URL 
		  String decipher=request.getParameter("email");
		  String email=null;
		try {
			email = CipherHelper.decipher("emailEmp", decipher);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 // System.out.println("Email"+email);
		  EmployeeController control=new EmployeeController();
		  boolean activate=control.activeEmployee(email);
		 if(activate){
			   out.println("alert('Successfuly Registration , Thank you')");
			   out.println("location='/STsService/UI/firstPage.jsp';");
		 }else{
			  out.println("alert('Problem has ocurred , :( ')");   
		 }
		 out.println("</script>");  
	}
}
