package com.sts.service.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sts.service.algorithms.CipherHelper;
import com.sts.service.controller.OrderController;
import com.sts.service.dao.EmployeeDAO;
/**
 * Servlet implementation class VerifyOrders
 */
@WebServlet("/VerifyOrders")
public class VerifyOrders extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VerifyOrders() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		OrderController controll=new OrderController();
		PrintWriter out =response.getWriter();
		out.println("<script type=\"text/javascript\">");
		try{
		
		String id=request.getParameter("id");
		String x=CipherHelper.decipher("ordeerID", id);
		int orderID=Integer.parseInt(x);
		
       // System.out.println(email+orderID);	
		boolean isConfim=controll.activeOrder(orderID);
		if(isConfim){
		out.println("alert('Thanks ,your order was confirmed :) ')");
		 out.println("location='/STsService/UI/firstPage.jsp';");
		}else{
			out.println("alert('problem has occurred");
		}
		}
		catch(Exception e){
			e.printStackTrace();
			out.println("alert('problem has occurred");
		}
		finally{
		    out.println("</script>");}
		
		
	}

}
