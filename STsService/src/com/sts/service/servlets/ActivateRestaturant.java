package com.sts.service.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sts.service.controller.EmployeeController;
import com.sts.service.controller.RestaurantsController;
import com.sts.service.dao.RestaurantDAO;
import com.sts.service.module.Restaurants;

@WebServlet("/ActivateRestaturant")
public class ActivateRestaturant extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ActivateRestaturant() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("radio");
	   RestaurantsController controll=new RestaurantsController();
	  Restaurants  res=controll.selectRes(name);
	    controll.updateAll();
	    controll.updateRow(res);
	     
	        PrintWriter out =response.getWriter();
			out.println("<script type=\"text/javascript\">");
			out.println("alert('Thank you ')");
			out.println("location='/STsService/UI/AdminForm1.jsp';");
		    out.println("</script>");

	     
	 //System.out.println("name of selected res :"+res);
		
	}

}
