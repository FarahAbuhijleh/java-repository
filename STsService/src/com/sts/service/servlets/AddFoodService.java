package com.sts.service.servlets;
import java.io.IOException;
import java.io.PrintWriter;


import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sts.service.controller.MenuController;
import com.sts.service.controller.RestaurantsController;
import com.sts.service.dao.MenuDAO;
import com.sts.service.dao.RestaurantDAO;
import com.sts.service.module.Menu;
import com.sts.service.module.Restaurants;


/**
 * Servlet implementation class AddServiceController
 */
@WebServlet("/AddFoodService")
public class AddFoodService extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final String ACTIVE="0";   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddFoodService() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter out =response.getWriter();
		out.println("<script type=\"text/javascript\">");
		
		String resName=request.getParameter("resName");
		String name=request.getParameter("mealName");
		String p=request.getParameter("price");
		String type=request.getParameter("type");
		Double price=0.0;
		price=Double.parseDouble(p);
		
		//System.out.println("name :"+name+"nameRes :"+resName+ " price "+price+ " Type "+type);

		RestaurantsController controller=new RestaurantsController();
		MenuController menuCon=new MenuController();
		Restaurants res=null;
		int id=controller.selectID(resName, ACTIVE);
		if(id==-1){
			out.println("alert('problem has occurred");
			out.println("location='/STsService/UI/AddFoodForm.jsp';");	
		}
		else if(id==0){	
			 res=new Restaurants(resName,ACTIVE);
			boolean addRest=controller.addRestaurant(res);
			if(addRest){
				boolean isAdded=menuCon.addMenuToRest(name, price, type,res);}
			else{
				out.println("alert('problem has occurred')");
				out.println("location='/STsService/UI/AddFoodForm.jsp';");}
			
		}else{
			res=new Restaurants(id,resName,ACTIVE);
		}
		boolean isAdded=menuCon.addMenuToRest(name, price, type,res);
		if(isAdded){
			out.println("alert('The "+name +"  was successfully added into "+resName+"')");
			out.println("location='/STsService/UI/AddFoodForm.jsp';");}
		else{
			out.println("alert('problem has occurred')");
			out.println("location='/STsService/UI/AddFoodForm.jsp';");}
		
		 out.println("</script>");}
}