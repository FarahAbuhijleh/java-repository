package com.sts.service.servlets;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sts.service.algorithms.CipherHelper;
import com.sts.service.controller.EmployeeController;



@WebServlet("/SignUpController")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final String active="0";   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Get  request parameters 
	    	String email=request.getParameter("email");
	    	String department=request.getParameter("department");
		    String password=request.getParameter("password");
		//Create object of SignUpController    
		    EmployeeController  control=new EmployeeController();
		//Encrypt password By Using Base64Encoder    
		 //   String pass=control.encryptPassword(password);
		//Check if email is Exist 
		    String pass=null;
			try {
				pass = CipherHelper.cipher("password",password);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    boolean check=control.checkEmail(email);
		    PrintWriter out =response.getWriter();
		    out.println("<script type=\"text/javascript\">");
		//True , the Email is not Exist    
		    if(check){
		    boolean isAdded= control.addEmployee(email, department, pass,active);
		    if(isAdded){
		    	 //Send Email to user to activate his account
		    	 boolean isSend=control.sendEmail(email); 
			     if(isSend){
			    	  out.println("alert('Check email to activate your account')");
			          out.println("location='/STsService/UI/firstPage.jsp';");}
			     else{
			    	 out.println("alert('Problem has occurred')"); 
			     }
			     }
		    else{
		    	  out.println("alert('Problem has occurred')"); }
		    }else{
				out.println("alert('Email already Exist ')");
				out.println("location='/STsService/UI/SignUp.jsp';");
		    }
		    out.println("</script>");}
}
