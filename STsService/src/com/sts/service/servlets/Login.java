package com.sts.service.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sts.service.algorithms.CipherHelper;
import com.sts.service.controller.EmployeeController;



@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public Login() {
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String email=request.getParameter("email");
		String pass=request.getParameter("password");
		PrintWriter out =response.getWriter();
		out.println("<script type=\"text/javascript\">");
		
		//Encrypt password 
		EmployeeController controllerEmp=new EmployeeController();
	//	String password=controllerEmp.encryptPassword(pass);
		//Check if the the user have account in dataBase Entry 
		
		if(email==null){
			RequestDispatcher dispatcher=request.getRequestDispatcher("/menu");
			dispatcher.forward(request, response);
		}
		else{
		
		boolean exist=controllerEmp.checkEmail(email);
		System.out.println(exist);
		if(!exist){
		String passwordEn=controllerEmp.Selectpass(email);
		String password=null;
		try {
			password=CipherHelper.decipher("password", passwordEn);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(password);
		System.out.println(pass);
		if(password.equals(pass)){
			 System.out.println("equal");
		boolean checkEmp=controllerEmp.checkEmployee(email,passwordEn);
		 System.out.println(checkEmp);
		if(checkEmp){
		    System.out.println("exist");
			int checkActive =controllerEmp.checkActiveOfEmail(email, password);
			if(checkActive==1){
				System.out.println("active");
			request.getSession().setAttribute("email", email);
			request.setAttribute("email", email);
			RequestDispatcher dispatcher=request.getRequestDispatcher("/menu");
			dispatcher.forward(request, response);
			
			}else if (checkActive==0){
				System.out.println("not active");
				out.println("alert('please Check your email to active your account ')");
				out.println("location='/STsService/UI/Form.jsp';");
			}else{
				out.println("alert('problem has occurred')");
			}
		}else{
			out.println("alert('Sorry, we do not recognize this email ')");
			out.println("location='/STsService/UI/Form.jsp';");
		}
		
		}else{
			out.println("alert('Password does not Correct,Try again')");
			out.println("location='/STsService/UI/Form.jsp';");
		}
		
		}
		else{
			System.out.println("Does not exist");
			out.println("alert('sorry, we do not recogonize this email ')");
			out.println("location='/STsService/UI/Form.jsp';");
		}
		 out.println("</script>");
	}

	}
}