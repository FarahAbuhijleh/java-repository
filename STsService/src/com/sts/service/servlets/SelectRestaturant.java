package com.sts.service.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sts.service.controller.RestaurantsController;
import com.sts.service.dao.RestaurantDAO;
import com.sts.service.module.Restaurants;

/**
 * Servlet implementation class SelectResController
 */
@WebServlet("/SelectRestaturant")
public class SelectRestaturant extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectRestaturant() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    RestaurantsController resController=new RestaurantsController();
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<Restaurants>list=resController.selectAll();
		request.setAttribute("listRes", list);
    	RequestDispatcher dispatcher=request.getRequestDispatcher("UI/indicateRestaurant.jsp");
		dispatcher.forward(request, response);
	}

}
