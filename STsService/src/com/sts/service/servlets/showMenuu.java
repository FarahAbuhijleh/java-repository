package com.sts.service.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sts.service.controller.RestaurantsController;
import com.sts.service.module.Restaurants;

/**
 * Servlet implementation class showMenuu
 */
@WebServlet("/showMenuu")
public class showMenuu extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public showMenuu() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RestaurantsController controll=new RestaurantsController();
		Restaurants res=controll.selectResActive();
		request.setAttribute("Restaturant", res);
		
		    RequestDispatcher dispatcher=request.getRequestDispatcher("UI/OrderPage.jsp");
			dispatcher.forward(request, response);
				
		
		
	}

}
