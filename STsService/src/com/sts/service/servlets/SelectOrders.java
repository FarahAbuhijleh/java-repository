package com.sts.service.servlets;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.SendResult;
import javax.xml.ws.Dispatch;




import com.sts.service.controller.EmployeeController;
import com.sts.service.controller.MenuController;
import com.sts.service.controller.OrderController;
import com.sts.service.controller.RestaurantsController;
import com.sts.service.dao.EmployeeDAO;
import com.sts.service.module.Email;
import com.sts.service.module.Employee;
import com.sts.service.module.Menu;
import com.sts.service.module.Restaurants;

@WebServlet("/SelectOrders")
public class SelectOrders extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final String CONFIRM="0";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectOrders() {
        super();
        // TODO Auto-generated constructor stub
    }	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
				
		//System.out.println("order select");	
	    MenuController menuCon=new MenuController();
	    OrderController orderCont=new OrderController();
		String email=(String) request.getSession().getAttribute("email");
		
		String[] sandwich=request.getParameterValues("sandwich");
		String [] meal=request.getParameterValues("meal");
		String [] salad=request.getParameterValues("salad");
		String [] juice=request.getParameterValues("juice");
		
		EmployeeController controll=new EmployeeController();
		Employee emp=controll.selectEmp(email);
		System.out.println(emp.getId());
		
	    int sandwichID=0,saladID=0,juiceID=0,mealID=0;
	    double totalPrice=0.0;
	    String Order="";
	    
		if(sandwich!=null){
	for(int i=0;i<sandwich.length;i++){
		 sandwichID=Integer.parseInt(sandwich[i]);
		 Menu menu=menuCon.selectMenu(sandwichID);
		 Order+="\n"+menu.getName();
		 totalPrice+=menu.getPrice();
		 System.out.println(menu);
	}}
		if(salad!=null){
	for(int i=0;i<salad.length;i++){
		saladID=Integer.parseInt(salad[i]);
		 Menu menu=menuCon.selectMenu(saladID);
		 Order+="\n"+menu.getName();
		 totalPrice+=menu.getPrice();
		//System.out.println(saladID);
	}}
	if(juice!=null){
	for(int i=0;i<juice.length;i++){
		juiceID=Integer.parseInt(juice[i]);
		 Menu menu=menuCon.selectMenu(juiceID);
		 Order+="\n"+menu.getName();
		 totalPrice+=menu.getPrice();
		//System.out.println(juiceID);
	}}
	if(meal!=null){
		for(int i=0;i<meal.length;i++){
		   	 mealID=Integer.parseInt(meal[i]);
			 Menu menu=menuCon.selectMenu(mealID);
			 Order+="\n"+menu.getName();
			 totalPrice+=menu.getPrice();
			
		//	System.out.println(mealID);
		}}

         //System.out.println(totalPrice);
        // System.out.println(Order);
	 int x=0;
	  PrintWriter out =response.getWriter();
	  out.println("<script type=\"text/javascript\">");
	  if(Order.equals("")){
		  out.println("alert('Please Choose your order  ^^')");
		//  System.err.println("cjeeeeeeek");
		// RequestDispatcher dispatcher=request.getRequestDispatcher("/menu");
		// dispatcher.forward(request, response);
			 
		 
	  }else{
	  int isAdded=orderCont.addOrder(emp, Order, CONFIRM,new Date());
	  System.out.println("date :"+new Date());

	  
	  if(isAdded!=-1){
		 
		 boolean isSend=orderCont.sendEmail(email,isAdded,Order,totalPrice);
		 if(isSend){
			 out.println("alert('Check your email to confirm your Order ^^ , Thanks')");
			 out.println("location='/STsService/UI/firstPage.jsp';");
		 }
		 else{
			 out.println("alert('problem has occurred')");
		 }
	  }else{
		  out.println("alert('problem has occurred')");
	  }
	  out.println("</script>");
	}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		
	}
}
		
		
	
