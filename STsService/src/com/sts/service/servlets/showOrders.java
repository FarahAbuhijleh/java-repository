package com.sts.service.servlets;

import java.io.IOException;
import java.util.List;




import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sts.service.controller.OrderController;
import com.sts.service.module.Order;

/**
 * Servlet implementation class showOrders
 */
@WebServlet("/showOrders")
public class showOrders extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public showOrders() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		OrderController controll=new OrderController();
		List<Order>list=controll.selectOrder();
		/*for(Order x:list){
			System.out.println("Name : "+x.getEmployee().getEmail()+"Order"+x.getOrders());
		}*/
		request.setAttribute("OrderList", list);
		RequestDispatcher dispatcher=request.getRequestDispatcher("UI/showOrders.jsp");
		dispatcher.forward(request, response);
	}

}
